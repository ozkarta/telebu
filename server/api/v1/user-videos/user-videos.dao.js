'use strict'
const Model = require('./user-videos.model').model;
const Promise = require('bluebird');

module.exports = {
  getById,
  getByQuery,

  create,
  updateOne,

  deleteByQuery,
}

function getById(userId) {
  return Model.findById(userId, PRIVATE_FIELDS).lean();
}

function getByQuery({find = {}, or = [{}], sort = {_id: -1}, offset, limit}) {
  return Promise.all([
    Model.find(find).lean().or(or).sort(sort).skip(offset).limit(limit),
    Model.find(find).lean().or(or).countDocuments()
  ])
  .spread((items, numTotal) => ({ items, numTotal }));
}

function create(data) {
  return Model.create(data);
}

function updateOne(id, data) {
  return Model.findOneAndUpdate({userId: id}, data).lean();
}

function deleteByQuery(query) {
  return Model.deleteOne(query);
}
