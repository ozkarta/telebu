const mongoose = require('mongoose');

let userVideoSchema = new mongoose.Schema({
  title: {type: String, trim: true, required: true},
  url: {type: String, trim: true, required: true},
  userId: {type: String, trim: true, required: true},  
}, {
  timestamps: true
});

let userVideoModel = mongoose.model('UserVideo', userVideoSchema);

exports.model = userVideoModel;
exports.schema = userVideoSchema;