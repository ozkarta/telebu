'use strict'
const bcrypt = require('bcryptjs');
const UserVideoDao = require('./user-videos.dao');

const {
  AppError,
  ResourceNotFoundError,
  UnauthorizedError,
  GrantError,
  ValidationError,
  DeactivatedSessionError
} = require('../../../shared/errors');
module.exports = {
  queryUserVideo,
  createUserVideo,
  updateUserVideo,
  deleteUserVideo,
}


async function queryUserVideo(req, res, next) {
  try {
    const {searchQuery} = req;
    const result = await UserVideoDao.getByQuery(searchQuery);
    return res.status(200).json(result);
  } catch (error) {
    return next(error);
  }
}

async function createUserVideo(req, res, next) {
  try {
    let { title, url } = req.body;
    let userVideo = { title, url, userId: req.userId };

    let result = await UserVideoDao.create(userVideo);
    return res.status(200).json(result);
  } catch (error) {
    next(error);
  }
}

async function updateUserVideo(req, res, next) {
  try {
    let { title, url, _id } = req.body;
    let userVideo = { title, url };

    user = await UserVideoDao.updateOne(_id, userVideo);
    return res.status(200).json({});
  } catch (error) {
    next(error);
  }
}

async function deleteUserVideo(req, res, next) {
  try {
    const {id: _id} = req.params;
    await UserVideoDao.deleteByQuery({_id});
    return res.status(200).json({});
  } catch (error) {
    return next(error);
  }
}
// ========================================================================================