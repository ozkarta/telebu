'use strict'
const { ValidationError } = require('../../../shared/errors');
const { parseOffsetAndLimit, parseSortBy } = require('../../../shared/helpers/parser');

module.exports = {
  parseQueryUserVideos,
  parseCreateUserVideo,
  parseUpdateUserVideo,
  parseDeleteUserVideo,
}

function parseCreateUserVideo(req, res, next) {
  try {
    if (!req.body) next(new ValidationError('Body is required'));
    let {title, url} = req.body;
    req.parsed = {
      title, url
    };
    return next();
  } catch (error) {
    return next(error);
  }
}

function parseQueryUserVideos(req, res, next) {
  try {
    const { query } = req;
    req.searchQuery = {
      ...parseOffsetAndLimit(query),
      ...parseSortBy(query),
      find: {
        userId: req.userId
      }
    }
    next();
  } catch (error) {
    return next(error);
  }
}

function parseUpdateUserVideo(req, res, next) {
  try {
    if (!req.body) next(new ValidationError('Body is required'));
    let {title, url} = req.body;

    if (!req.params || !req.params.id) next(new ValidationError('ID is required'));
    let {id} = req.params;

    req.parsed = {
      title, url, _id: id
    };
    return next();
  } catch (error) {
    return next(error);
  }
} 

function parseDeleteUserVideo(req, res, next) {
  try {
    if (!req.params || !req.params.id) next(new ValidationError('ID is required'));
    let {id} = req.params;

    req.parsed = {
      _id: id
    };
    return next();
  } catch (error) {
    return next(error);
  }
}