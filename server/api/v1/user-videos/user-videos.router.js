'use strict'
const UserVideosService = require('./user-videos.service');
const router = require('express').Router();
const { ClientGuard } = require('../../../shared/auth/auth');
const Parser = require('./user-videos.parser');



router.get('/', ClientGuard, Parser.parseQueryUserVideos, UserVideosService.queryUserVideo);
router.post('/', ClientGuard, Parser.parseCreateUserVideo, UserVideosService.createUserVideo);
router.put('/:id', ClientGuard, Parser.parseUpdateUserVideo, UserVideosService.updateUserVideo);
router.delete('/:id', ClientGuard, Parser.parseDeleteUserVideo, UserVideosService.deleteUserVideo);

module.exports = router;