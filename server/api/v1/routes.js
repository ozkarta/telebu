'use strict'
const express = require('express');
const router = express.Router();
const Auth = require('../../shared/auth/auth');

router.use(Auth.setUser);

module.exports = {
  mainServer: () => {
    router.use('/user-videos', require('./user-videos/user-videos.router'));
    return router;
  }
}