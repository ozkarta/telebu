'use strict';

class AppError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
  }
}

class ResourceNotFoundError extends AppError {
  constructor(message) {
    super(message);
    this.httpStatusCode = 500;
  }
}

class UnauthorizedError extends AppError {
  constructor(message) {
    super(message);
    this.httpStatusCode = 401;
  }
}

class GrantError extends AppError {
  constructor(message) {
    super(message);
    this.httpStatusCode = 403;
  }
}

class ValidationError extends AppError {
  constructor(message) {
    super(message);
    this.httpStatusCode = 400;
  }
}

class DeactivatedSessionError extends AppError {
  constructor(message) {
    super(message);
    this.httpStatusCode = 401;
  }
}

module.exports = {
  AppError,
  ResourceNotFoundError,
  UnauthorizedError,
  GrantError,
  ValidationError,
  DeactivatedSessionError
};
