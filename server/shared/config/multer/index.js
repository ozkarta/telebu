'use strict';

const multer = require('multer');
const config = require('../environment');
const path = require('path');

const limits = {
  fieldSize: 10000000,
  fileSize: 10000000,
};

const imageFileTypes = /jpeg|jpg|png|svg/;
const videoFileTypes = /webm|mp4|/;

function imageFileFilter(req, file, cb) {
  let mimeType = imageFileTypes.test(file.mimetype);
  let extName = imageFileTypes.test(path.extname(file.originalname).toLowerCase());

  if (mimeType && extName) {
    return cb(null, true);
  } else {
    cb('Error: File upload only supports the following filetypes - ' + imageFileTypes, false);
  }
}

function videoFileFilter(req, file, cb) {
  let mimeType = videoFileTypes.test(file.mimetype);
  let extName = videoFileTypes.test(path.extname(file.originalname).toLowerCase());

  if (mimeType && extName) {
    return cb(null, true);
  } else {
    cb('Error: File upload only supports the following filetypes - ' + videoFileTypes, false);
  }
}

function getMulterConfigForImages() {
  const storage = multer.diskStorage({
    destination: config.paths.imageUploads,
  
    filename: (req, file, cb) => {
      let extName = path.extname(file.originalname).toLowerCase()
      cb(null, `${require('../../helpers/util').generateRandomString(20)}-${(new Date()).toISOString()}${extName}`);
    }
  });

  return {
    storage,
    limits,
    fileFilter: imageFileFilter
  }
}

function getMulterConfigForVideo() {
  const storage = multer.diskStorage({
    destination: config.paths.imageUploads,
  
    filename: (req, file, cb) => {
      cb(null, file.originalname);
    }
  });

  return {
    storage,
    limits,
    fileFilter: videoFileFilter
  }
}


module.exports = {
  getMulterConfigForImages,
  getMulterConfigForVideo,
};
