'use strict';

const path = require('path');

const homePath = process.env.HOME || process.env.HOMEPATH;
const dataRoot = path.join(homePath, '.markethub-server-data');
const env = process.env.NODE_ENV || 'development';

const config = {
  appName: 'market-hub',
  env,
  paths: {
    root: dataRoot,
    log: path.join(dataRoot, 'logs'),
    imageUploads: path.join(dataRoot, 'uploads/images'),
    videoUploads: path.join(dataRoot, 'uploads/video'),
    otherUploads: path.join(dataRoot, 'uploads/other'),
  },
  fileConfig: {
    image: {
      resolutions: {
        lowResolutionImage: {
          filePrefix: 'low',
          width: 120,
          height: 160,
        },
        mediumResolutionImage: {
          filePrefix: 'medium',
          width: 480,
          height: 640,
        },
        highResolutionImage: {
          filePrefix: 'high',
          width: 960,
          height: 1280,
        },
        // extraLarge: {
        // filePrefix: 'extra-high',
        //   width: 960,
        //   height: 1128,
        // },
      }
    }
  }
}

module.exports = Object.assign({}, config, require(`./${env}.js`));