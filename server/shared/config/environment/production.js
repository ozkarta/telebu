'use strict';
module.exports = {
  enableCORS: true,
  mongoURL: 'mongodb://localhost/markethub',
  PORT: 5000,
  seedDB: false,
  appSecret: 'markethub_ozkart_app_secret_12jsdJsln@213nmdnfFasdl@3))skKJHfasdklH',
  JWT_TOKEN_EXPIRATION: 1 * 365 * 24 * 60 * 60 * 1000, // 1 year

  allowedOrigins: [
    'http://markethub.ge',
    'https://markethub.ge',

    'http://server.markethub.ge',
    'https://server.markethub.ge',
  ]
}