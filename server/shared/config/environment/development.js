'use strict';
module.exports = {
  enableCORS: true,
  mongoURL: 'mongodb://localhost/telebu-ozkart-app',
  PORT: 3000,
  seedDB: false,
  appSecret: 'telebu_ozkart_app_secret_12jsdJsln@213nmdnfFasdl@3))skKJHfasdklH',
  JWT_TOKEN_EXPIRATION: 1 * 365 * 24 * 60 * 60 * 1000, // 1 year

  allowedOrigins: [
    'http://localhost:4200',
    '*',
  ],
}