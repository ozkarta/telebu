'use strict';
const logger = require('./logger');

process.on('uncaughtException', handleError);
process.on('unhandledRejection', handleError);

function handleError(err) {
  const content = 'Uncaught Error! - ' + JSON.stringify(err.stack || err);

  logger.error(content);
  console.log(err);
  process.exit(1);
}
