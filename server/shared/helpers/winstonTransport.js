const Transport = require('winston-transport');
const fs = require('fs');
const util = require('util');
const config = require('../config/environment');
const path = require('path');

module.exports = class WinstonTransport extends Transport {

  constructor(opts) {
    super(opts);

    this.initFilesAndFolders();
    this.startRotationInterval();
  }

  initFilesAndFolders() {
    this.initRootDirectory();
    this.initLogFolderByDate();
    this.initLogFile();
  }

  startRotationInterval() {
    if (this.activeInterval) {
      clearInterval(this.activeInterval);
    }
    let msDiff = this.endOfTheDay.getTime() - this.lastRotateDate.getTime();
    this.activeInterval = setInterval(() => {
      this.initFilesAndFolders();
      this.startRotationInterval();
    }, msDiff);
  }

  initRootDirectory() {
    if (!fs.existsSync(config.paths.root)) {
      fs.mkdirSync(config.paths.root);
    }

    if (!fs.existsSync(config.paths.log)) {
      fs.mkdirSync(config.paths.log);
    }
  }

  initLogFolderByDate() {
    this.lastRotateDate = new Date();
    this.endOfTheDay = new Date();
    this.endOfTheDay.setHours(24, 0, 0, 0);

    this.logFileParentDirName = this.yyyymm(this.lastRotateDate);
    this.logFileParentDirPath = path.join(config.paths.log, this.logFileParentDirName);
    if (!fs.existsSync(this.logFileParentDirPath)) {
      fs.mkdirSync(this.logFileParentDirPath);
    }
  }

  initLogFile() {
    this.appendFileSync('', false);
  }

  appendFileSync(content, newLine = true) {
    if (newLine) {
      content = `${content}\n`;
    }
    this.logFileName = `${config.appName}-${config.env}-${this.yyyymmdd()}.log`;
    this.logFilePath = path.join(this.logFileParentDirPath, this.logFileName);
    fs.appendFileSync(this.logFilePath, content, 'utf8');
  }

  log(info, callback) {
    setImmediate(() => {
      this.emit('logged', info);
    });

    if (!this.lastRotateDate || this.lastRotateDate.getDate() !== (new Date()).getDate()) {
      this.initRootDirectory();
      this.initLogFolderByDate();
      this.startRotationInterval();
    }
    if (!info) info = {};
    info.createDate = new Date();
    this.appendFileSync(JSON.stringify(info));
    callback();
  }

  yyyymm(date = new Date()) {
    let mm = date.getMonth() + 1; // getMonth() is zero-based
    // var dd = date.getDate();

    return [
      date.getFullYear(),
      (mm > 9 ? '' : '0') + mm
    ].join('-');
  };

  yyyymmdd(date = new Date()) {
    let mm = date.getMonth() + 1; // getMonth() is zero-based
    var dd = date.getDate();

    return [
      date.getFullYear(),
      (mm > 9 ? '' : '0') + mm,
      (dd > 9 ? '' : '0') + dd
    ].join('-');
  };
}