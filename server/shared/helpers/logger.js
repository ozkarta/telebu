'use strict';
const winston = require('winston');
const WinstonTransport = require('./winstonTransport');

const transport = new WinstonTransport();

var logger = winston.createLogger({
  transports: [
    transport
  ]
});

module.exports = logger;
