'use strict';
const config = require('../config/environment');
const logger = require('./logger');

module.exports = (err, req, res, next) => {
  if (!err) return next();
  if (config.env === 'development') {
    console.log(err);
  }
  logger.error(JSON.stringify(err.stack || err));
  return res.status(500).send({name: err.name, message: err.message, code: err.httpStatusCode});
}