
const {GrantError, UnauthorizedError, ResourceNotFoundError, DeactivatedSessionError} = require('../errors');

module.exports.setUser = async function(req, res, next) {
	let userId = req.headers['userid'];
	if (!userId) return next();
	req.userId = userId;
	return next();
}

module.exports.ClientGuard = async function(req, res, next) {
	if (!req.userId) {
		return next(new UnauthorizedError(`You are not Authorized`));
	}
	return next();
};