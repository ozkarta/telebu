// Get dependencies
global.__basedir = __dirname;

const express = require('express');
const http = require('http');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const timeout = require('connect-timeout');
const Promise = require('bluebird');
const cors = require('cors');
const env = process.env.NODE_ENV || 'development';
const config = require('./shared/config/environment');

class Server {
  constructor() {
    this.options = {
      origin: (origin, callback) => {
        if (config.allowedOrigins.indexOf(origin || '*') !== -1) {
          callback(null, true)
        } else {
          callback(new Error('Not allowed by CORS'))
        }
      }
    }
    this.app = express();
    this.server = http.Server(this.app);
    this.v1Routes = require('./api/v1/routes').mainServer();
    this.db = mongoose.connection;
    this.initDB();
    this.port = config.PORT;
    this.initApp();
    this.server.listen(this.port, () => console.log(`Our server is running on: ${this.port}`));

    this.io = require('socket.io')(this.server, {
      pingInterval: 10000,
      pingTimeout: 5000
    });
  }

  initDB() {
    mongoose.set('useFindAndModify', false);
    mongoose.set('useUnifiedTopology', true);
    mongoose.Promise = Promise;
    if (process.env.mongoURL || config.mongoURL) {
      const mongoUrl = process.env.mongoURL || config.mongoURL;
      mongoose.connect(mongoUrl, { useNewUrlParser: true });
      const db = mongoose.connection;
      db.on('error', (err) => {
        console.error(err);
        console.log(mongoUrl);
      });
      db.once('open', () => {
        console.info('Connected to the database');
        console.log(mongoUrl);
      });
    } else {
      console.log('DB connection string was not found.');
    }
  }

  initApp() {
    // Parsers for POST data
    this.app.use(cookieParser());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));
    
    if (config.enableCORS) {
      this.app.use(cors(this.options));
    }
    

    // Set our api routes
    this.app.use('/api/v1', this.v1Routes);


    this.app.use(require('./shared/helpers/express-error-handler'));

    // Catch all other routes and return 404
    this.app.use('*', (req, res) => {
      return res.sendStatus(404);
    });

    this.app.set('port', this.port);



    this.app.use(timeout(120000));
    this.app.use(this.haltOnTimedout);
  }

  haltOnTimedout(req, res, next){
    if (!req.timedout) next();
  }
}


require('./shared/helpers/uncaught-error-handler');
const server = new Server();