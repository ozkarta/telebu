import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
const API_URL = environment['serverURL'];
@Injectable()
export class UserVideoAPIService {
  constructor(private httpClient: HttpClient) { }

  getUserVideos(): Observable<any> {
    return this.httpClient.get<any>(`${API_URL}/api/v1/user-videos`);
  }

  addUserVideo(videoObject: any): Observable<any> {
    return this.httpClient.post<any>(`${API_URL}/api/v1/user-videos`, videoObject);
  }

  updateUserVideo(videoId: any, videoObject: any): Observable<any> {
    return this.httpClient.put<any>(`${API_URL}/api/v1/user-videos/${videoId}`, videoObject);
  }

  deleteUserVideo(videoId: any): Observable<any> {
    return this.httpClient.delete<any>(`${API_URL}/api/v1/user-videos/${videoId}`);
  }
}