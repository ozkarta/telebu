import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError as obsThrowError } from 'rxjs';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { catchError, } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class  AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService,
    private router: Router) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authParams = this.authService.getAuthParams();
    let request: any = req.clone({
      setHeaders: authParams
    });

    return next.handle(request).pipe(catchError((err: HttpErrorResponse) => {
      if (err.status === 401) {
        this.router.navigate(['/']);
        return obsThrowError(err);
      } else {
        return obsThrowError(err);
      }
    }));
  }
}