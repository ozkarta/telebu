import { Injectable } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject, of, zip } from 'rxjs';
import { map, catchError, concatMap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()

export class AuthService {
  private currentUserObject: any = null;
  private _currentUser: BehaviorSubject<any> = new BehaviorSubject(this.currentUserObject);

  public currentUser: Observable<any> = this._currentUser.asObservable();

  private isSignedObject: boolean = false;
  private _isSigned: BehaviorSubject<boolean> = new BehaviorSubject(this.isSignedObject);

  public isSigned: Observable<boolean> = this._isSigned.asObservable();

  constructor(private router: Router) {

  }

  setUser(data: any) {
    console.log(data);
    this.currentUserObject = data;
    this._currentUser.next(data);
  }

  setIsSigned(data: boolean) {
    console.log(data);
    this.isSignedObject = data;
    this._isSigned.next(data);
  }


  isSignedUser(): boolean {
    return this.isSignedObject;
  }

  getAuthParams() {
    return {
      userId: this.currentUserObject.id
    }
  }

}