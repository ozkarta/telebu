import { Injectable, NgZone } from '@angular/core';
import { Subject, Observable, Observer, BehaviorSubject, of, zip } from 'rxjs';
import { environment } from '../../../environments/environment';
import { AuthService } from './auth.service';
declare var gapi: any;
@Injectable()
export class YoutubeAPIService {
  private _youtubeIframeAPIReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public youtubeIframeAPIReady = this._youtubeIframeAPIReady.asObservable();

  private _GoogleAuth: any;
  private apiKey: string = environment['google']['web']['apiKey'];
  private clientId: string = environment['google']['web']['client_id'];
  private discoveryDocs: string[] = ['https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest'];
  private scope: string = [
    'https://www.googleapis.com/auth/youtube',
    'https://www.googleapis.com/auth/youtube.channel-memberships.creator',
    'https://www.googleapis.com/auth/youtube.force-ssl',
    'https://www.googleapis.com/auth/youtube.readonly',
    'https://www.googleapis.com/auth/youtube.upload',
    'https://www.googleapis.com/auth/youtubepartner',
    'https://www.googleapis.com/auth/youtubepartner-channel-audit',
    'https://www.googleapis.com/auth/userinfo.profile',
  ].join(' ');

  private isAuthorized: boolean = false;;
  private currentApiRequest: any;

  constructor(private autService: AuthService,
    private zone: NgZone) {
  }

  load() {
    if (window['YT']) {
      this._youtubeIframeAPIReady.next(true);
    } else {
      window['onYouTubeIframeAPIReady'] = () => {
        this._youtubeIframeAPIReady.next(true);
      };
    }
    
    gapi.load('client', this.start.bind(this));
  }

  private start() {
    gapi.client.init({
      apiKey: this.apiKey,
      discoveryDocs: this.discoveryDocs,
      clientId: this.clientId,
      scope: this.scope
    }).then(() => {
      this._GoogleAuth = gapi.auth2.getAuthInstance();

      // Listen for sign-in state changes.
      this._GoogleAuth.isSignedIn.listen(this.updateSigninStatus.bind(this));

      this.setAuthServiceProperties();
    });
  }

  private setAuthServiceProperties() {
    this.zone.run(() => {
      let gapiUser = this._GoogleAuth.currentUser.get().getBasicProfile();
      if (gapiUser) {
        this.autService.setUser({
          id: gapiUser.getId(),
          name: gapiUser.getName(),
          givenName: gapiUser.getGivenName(),
          familiName: gapiUser.getFamilyName(),
          imageUrl: gapiUser.getImageUrl(),
          email: gapiUser.getEmail(),
        });
      } else {
        this.autService.setUser(null);
      }

      this.autService.setIsSigned(this._GoogleAuth.isSignedIn.get());
    })
  }

  private sendAuthorizedApiRequest(requestDetails) {
    this.currentApiRequest = requestDetails;
    if (this.isAuthorized) {
      // Make API request
      // gapi.client.request(requestDetails)

      // Reset currentApiRequest variable.
      this.currentApiRequest = {};
    } else {
      this._GoogleAuth.signIn();
    }
  }

  private updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
      this.isAuthorized = true;
      if (this.currentApiRequest) {
        this.sendAuthorizedApiRequest(this.currentApiRequest);
      }
    } else {
      this.isAuthorized = false;
    }

    this.setAuthServiceProperties();
  }

  handleAuthClick() {
    if (this._GoogleAuth.isSignedIn.get()) {
      // User is authorized and has clicked "Sign out" button.
      this._GoogleAuth.signOut();
    } else {
      // User is not signed in. Start Google auth flow.
      this._GoogleAuth.signIn();
    }
  }

  revokeAccess() {
    this._GoogleAuth.disconnect();
  }
}