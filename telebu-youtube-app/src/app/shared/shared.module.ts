import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Modules
import { AngularMaterialModule } from './material/mat-import.module';
// Services
import { YoutubeAPIService } from './services/youtube-api.service';
import { AuthService } from './services/auth.service';
// API
import { UserVideoAPIService } from './api/user-video-api.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AngularMaterialModule,
  ],
  exports: [
    // Modules
    CommonModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    AngularMaterialModule,

  ],
  entryComponents: [
  ],
  declarations: [
  ],
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
        ngModule: SharedModule,
        providers: [
            // Services
            YoutubeAPIService,
            AuthService,
            // API Service
            UserVideoAPIService,
            // GUARDS
        ]
    };
}
}
