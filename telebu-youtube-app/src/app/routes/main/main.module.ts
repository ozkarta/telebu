import { NgModule } from '@angular/core';

import { MainRoutingModule } from './main-routing.module';
// import { ComponentsModule } from './components/components.module';

import { MainComponent } from './main.component';
import { SharedModule } from '../../shared/shared.module';

// import { ParentCategoryResolver, CurrentUserResolver } from './main.resolver';

@NgModule({
  imports: [
    SharedModule,
    MainRoutingModule,
    // ComponentsModule,
  ],
  declarations: [
    MainComponent,
  ],
  providers: [
    // ParentCategoryResolver,
    // CurrentUserResolver
  ]
})
export class MainModule { }
