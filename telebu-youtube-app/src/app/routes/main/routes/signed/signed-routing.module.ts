import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignedComponent } from './signed.component';
import { SignedGuard } from './signed-guard.service';

const routes: Routes = [
  {
    path: '',
    component: SignedComponent,
    canActivate: [
      SignedGuard
    ],
    children: [
      {        
        path: '',
        redirectTo: 'youtube-task'
      },
      {        
        path: 'youtube-task',
        loadChildren: './routes/youtube-task/youtube-task.module#YoutubeTaskModule'
      },
      {
        path: 'form-builder-task',
        loadChildren: './routes/form-builder-task/form-builder-task.module#FormBuilderTaskModule'
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignedRoutingModule { }
