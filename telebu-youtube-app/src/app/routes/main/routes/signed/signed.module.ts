import { NgModule } from '@angular/core';
import { SignedComponent } from './signed.component';
import { SignedRoutingModule } from './signed-routing.module';
import { SignedGuard } from './signed-guard.service';
import { SharedModule } from '../../../../shared/shared.module';
@NgModule({
  imports: [
    SignedRoutingModule,
    SharedModule,
  ],
  declarations: [
    SignedComponent
  ],
  providers: [
    SignedGuard
  ]
})
export class SignedModule { }
