import { Component, OnInit, OnDestroy } from '@angular/core';
import { YoutubeAPIService } from 'src/app/shared/services/youtube-api.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-form-builder-task-component',
  templateUrl: './form-builder-task.component.html',
  styleUrls: ['./form-builder-task.component.css']
})

export class FormBuilderTaskComponent implements OnInit, OnDestroy {
  public formGroup: FormGroup;
  public formConfig: FormConfig[] = [
    {
      key: 'fName',
      autocomplete: false,
      type: 'text',
      label: 'First Name',
      placeholder: 'eg. Oz',
      styles: {'width': '45%', 'display': 'inline-block', 'margin': '5px 10px'},
      validators: [Validators.required],
    },
    {
      key: 'lName',
      autocomplete: false,
      type: 'text',
      label: 'Last Name',
      placeholder: 'eg. Kart',
      styles: {'width': '45%', 'display': 'inline-block', 'margin': '5px 10px'},
      validators: [Validators.required],
    },
    {
      key: 'isFullAge',
      defaultValue: false,
      type: 'switch-toggle',
      label: 'Are You 18+',
      validators: [Validators.required],
    },
    {
      key: 'age',
      type: 'number',
      label: 'Please type your age',
      placeholder: 'eg. 23',
      styles: {'width': '90%', 'display': 'inline-block', 'margin': '5px 10px'},
      validators: [Validators.required, Validators.min(15)],
    },
    {
      key: 'aboutYou',
      type: 'textarea',
      minRows: 200,
      label: 'Please tell us some short info about you',
      styles: {'width': '90%', 'display': 'inline-block', 'margin': '5px 10px'},
      validators: [Validators.required, Validators.minLength(50)],
    },
    {
      key: 'gender',
      type: 'select',
      label: 'What is your gender?',
      selectValues: [
        {key: 'Male', value: 'male'},
        {key: 'Female', value: 'female'},
        {key: 'Other', value: 'other'}
      ],
      styles: {'width': '90%', 'display': 'inline-block', 'margin': '5px 10px'},
      validators: [Validators.required, Validators.minLength(50)],
    },
    {
      key: 'vehicle',
      type: 'checkbox',
      label: 'Please Choose your vehicle',
      styles: {'margin': '5px 10px'},
      checkboxValues: [
        {key: 'bike', value: 'bike', viewValue: 'Do you have Bike?'},
        {key: 'car', value: 'car', viewValue: 'Do you have Car?'},
        {key: 'boat', value: 'boat', viewValue: 'Do you have Boat?'}
      ],
      validators: [],
    },
  ]
  constructor(private fb: FormBuilder) {

  }

  ngOnInit() {
    this.formGroup = this.fb.group(this.generateFormBuilderConfig());
  }

  generateFormBuilderConfig(): any {
    let conf = {};
    for (let formConfigItem of this.formConfig) {
      conf[formConfigItem['key']] = [
        formConfigItem.defaultValue || (
          formConfigItem.type === ('text' || 'textarea')? '' : (
            formConfigItem.type === 'checkbox'? {} : (
              formConfigItem.type === 'select'? [] : (
                formConfigItem.type === 'switch-toggle'? false : null
              )
            )
          )
        ),
        [...(formConfigItem.validators || [])],
        []
      ]
    }
    return conf;
  }

  ngOnDestroy() {
    
  }

  checkboxValueChange(configItem: FormConfig, key: string, value: any) {
    let obj = {};
    obj[key] =  value;

    this.formGroup.controls[configItem.key].setValue(
      Object.assign(this.formGroup.controls[configItem.key].value, obj)
    );
  } 

  submitForm() {
    console.log(this.formGroup.value);
  }
}

interface FormConfig {
  key: string;
  autocomplete?: boolean;
  minRows?: number; // textarea rows
  defaultValue?: string | string[] | number | number[] | boolean;
  selectValues?: {key: string, value: string}[];
  checkboxValues?: {key: string, value: string, viewValue: string}[];
  type: 'text' | 'textarea' | 'number' | 'switch-toggle' | 'select' | 'checkbox';
  label: string;
  placeholder?: string;
  validators?: Validators[];
  styles?: any;
}