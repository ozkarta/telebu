import { NgModule } from '@angular/core';
import { FormBuilderTaskComponent } from './form-builder-task.component';
import { VisitorRoutingModule } from './form-builder-task-routing.module';
import { FormBuilderTastGuard } from './form-builder-task-guard.service';
import { SharedModule } from '../../../../../../shared/shared.module';
@NgModule({
  imports: [
    VisitorRoutingModule,
    SharedModule
  ],
  declarations: [
    FormBuilderTaskComponent
  ],
  providers: [
    FormBuilderTastGuard
  ]
})
export class FormBuilderTaskModule { }
