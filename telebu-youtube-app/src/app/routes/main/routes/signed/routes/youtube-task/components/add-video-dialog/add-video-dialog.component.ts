import {  Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-video-dialog-component',
  templateUrl: './add-video-dialog.component.html',
  styleUrls: ['./add-video-dialog.component.css']
})

export class AddVideoDialogComponent {
  private formConfig: any = {};
  public formGroup: FormGroup;
  constructor(
    private fb : FormBuilder,
    public dialogRef: MatDialogRef<AddVideoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.formConfig = {
      title: ['', Validators.required],
      url: ['', Validators.pattern(/http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)?‌​[\w\?‌​=]*)?/)],
    }
    this.formGroup = this.fb.group(this.formConfig);
  }

  save() {
    this.dialogRef.close(this.formGroup.value);
  }
}
