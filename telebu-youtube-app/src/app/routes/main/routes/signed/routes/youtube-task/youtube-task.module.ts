import { NgModule } from '@angular/core';
import { YoutubeTaskComponent } from './youtube-task.component';
import { YoutubeTaskRoutingModule } from './youtube-task-routing.module';
import { YoutubeTaskGuard } from './youtube-task-guard.service';
import { SharedModule } from '../../../../../../shared/shared.module';
import { ComponentsModule } from './components/components.module';
@NgModule({
  imports: [
    YoutubeTaskRoutingModule,
    SharedModule,
    ComponentsModule,
  ],
  declarations: [
    YoutubeTaskComponent
  ],
  providers: [
    YoutubeTaskGuard
  ]
})
export class YoutubeTaskModule { }
