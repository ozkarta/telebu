import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { YoutubeTaskComponent } from './youtube-task.component';
import { YoutubeTaskGuard } from './youtube-task-guard.service';

const routes: Routes = [
  {
    path: '',
    component: YoutubeTaskComponent,
    canActivate: [
      YoutubeTaskGuard
    ],
    children: [
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class YoutubeTaskRoutingModule { }
