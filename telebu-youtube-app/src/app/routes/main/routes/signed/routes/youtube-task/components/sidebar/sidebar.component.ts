import { Component, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddVideoDialogComponent } from '../add-video-dialog/add-video-dialog.component';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})

export class SidebarComponent {
  @Input('userVideos') userVideos: any[];
  @Output('userVideoDeleteHandler') userVideoDeleteEmitter: EventEmitter<any> = new EventEmitter();
  @Output('createUserVideoHandler') createUserVideoEmitter: EventEmitter<any> = new EventEmitter();

  @Output('pauseVideoHandler') pauseVideoEmitter: EventEmitter<any> = new EventEmitter();
  @Output('stopVideoHandler') stopVideoEmitter: EventEmitter<any> = new EventEmitter();
  @Output('playVideoHandler') playVideoEmitter: EventEmitter<any> = new EventEmitter();

  @Output('pauseAllVideosHandler') pauseAllEmitter: EventEmitter<any> = new EventEmitter();
  @Output('stopAllVideosHandler') stopAllEmitter: EventEmitter<any> = new EventEmitter();
  @Output('playAllVideosHandler') playAllEmitter: EventEmitter<any> = new EventEmitter();

  constructor(
    public dialog: MatDialog
  ) {}

  openAddVideoDialog(): void {
    const dialogRef = this.dialog.open(AddVideoDialogComponent, {
      width: '40%',
      data: {}
    });

    dialogRef.afterClosed().subscribe(video => {
      if (!video) return;
      this.createUserVideoEmitter.emit(video);
    });
  }

  deleteVideo(video: any) {
    if (!video) return;
    this.userVideoDeleteEmitter.emit(video._id);
  }

  pauseVideo(video: any) {
    if (!video) return;
    this.pauseVideoEmitter.emit(video);
  } 

  stopVideo(video: any) {
    if (!video) return;
    this.stopVideoEmitter.emit(video);
  } 

  playVideo(video: any) {
    if (!video) return;
    this.playVideoEmitter.emit(video);
  } 

  pauseAll() {
    this.pauseAllEmitter.emit(true);
  } 

  stopAll() {
    this.stopAllEmitter.emit(true);
  } 

  playAll() {
    this.playAllEmitter.emit(true);
  } 
}