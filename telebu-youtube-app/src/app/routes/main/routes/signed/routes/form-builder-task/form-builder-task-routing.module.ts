import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormBuilderTaskComponent } from './form-builder-task.component';
import { FormBuilderTastGuard } from './form-builder-task-guard.service';

const routes: Routes = [
  {
    path: '',
    component: FormBuilderTaskComponent,
    canActivate: [
      FormBuilderTastGuard
    ],
    children: [
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisitorRoutingModule { }
