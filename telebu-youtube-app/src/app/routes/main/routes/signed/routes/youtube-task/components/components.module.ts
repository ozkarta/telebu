import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { SidebarComponent } from './sidebar/sidebar.component';
import { ContentComponent } from './content/content.component';
import { AddVideoDialogComponent } from './add-video-dialog/add-video-dialog.component';
import { YoutubePlayerComponent } from './youtube-player-component/youtube-player-component';
@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [
    SidebarComponent,
    ContentComponent,
    AddVideoDialogComponent,
    YoutubePlayerComponent,
  ],
  exports: [
    SidebarComponent,
    ContentComponent,
    YoutubePlayerComponent,
  ],
  providers: [
  ],
  entryComponents: [
    AddVideoDialogComponent
  ]
})
export class ComponentsModule { }
