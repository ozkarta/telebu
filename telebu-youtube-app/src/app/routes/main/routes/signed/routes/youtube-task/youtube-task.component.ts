import { Component, OnInit, OnDestroy } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { UserVideoAPIService } from 'src/app/shared/api/user-video-api.service';
import { map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: 'app-youtube-task-component',
  templateUrl: './youtube-task.component.html',
  styleUrls: ['./youtube-task.component.css']
})

export class YoutubeTaskComponent implements OnInit, OnDestroy {
  public VIDEO_STATES = {
    STOP: 'stop',
    PAUSE: 'pause' ,
    PLAY: 'play'
  }
  public userVideos: any[];
  constructor(private userVideoAPIService: UserVideoAPIService) {
  }

  ngOnInit() {
    this.loadUserVideos();
  }

  ngOnDestroy() {
    
  }

  private loadUserVideos() {
    this.userVideoAPIService.getUserVideos().subscribe(
      (result: any[]) => {
        this.userVideos = (result['items'] || []).map((video: any) => {
          video.stateObservable = new BehaviorSubject(this.VIDEO_STATES.STOP);
          return video;
        });
      }
    )
  }

  public addUserVideo(videoObject: any) {
    this.userVideoAPIService.addUserVideo(videoObject).pipe(
      map((result: any) => {
        this.loadUserVideos();
        return result;
      })
    ).subscribe();
  }

  public deleteUserVideo(videoId: string) {
    this.userVideoAPIService.deleteUserVideo(videoId).pipe(
      map((result: any) => {
        this.loadUserVideos();
        return result;
      })
    ).subscribe();
  }

  public stopAll() {
    this.userVideos.forEach((video: any) => {
      video.stateObservable.next(this.VIDEO_STATES.STOP);
    });
  }

  public pauseAll() {
    this.userVideos.forEach((video: any) => {
      video.stateObservable.next(this.VIDEO_STATES.PAUSE);
    });
  }

  public playAll() {
    this.userVideos.forEach((video: any) => {
      video.stateObservable.next(this.VIDEO_STATES.PLAY);
    });
  }

  public pauseVideo(video: any) {
    video.stateObservable.next(this.VIDEO_STATES.PAUSE);
  }

  public stopVideo(video: any) {
    video.stateObservable.next(this.VIDEO_STATES.STOP);
  }

  public playVideo(video: any) {
    video.stateObservable.next(this.VIDEO_STATES.PLAY);
  }
}