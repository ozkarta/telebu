import { Component, OnInit, OnDestroy, Input, ViewChild, OnChanges, NgZone } from '@angular/core';
import { Subscription } from 'rxjs';
import { YoutubeAPIService } from 'src/app/shared/services/youtube-api.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
declare var YT: any;
@Component({
  selector: 'app-youtube-player-component',
  styleUrls: ['youtube-player-component.css'],
  templateUrl: 'youtube-player-component.html'
})

export class YoutubePlayerComponent implements OnInit, OnDestroy, OnChanges {
  @Input('height') height: number;
  @Input('width') width: number;
  @Input('userVideos') userVideos: any[];

  public source: SafeResourceUrl;
  public youtubeIframeAPIReady: boolean = false;
  private subscription: Subscription = new Subscription();
  private videoStateSubscriptions: Subscription = new Subscription();
  constructor(
    private zone: NgZone,
    private sanitizer: DomSanitizer,
    private youtubeAPIService: YoutubeAPIService
  ) { }
  ngOnInit() {
    // this.getVideoSource();
    // this.playerId = `${this.videoId}`;
    this.subscribeYoutubeIframeAPIReady();
  }

  ngOnChanges(changes) {
    if (changes && changes.userVideos && this.youtubeIframeAPIReady) {
      this.videoStateSubscriptions.unsubscribe();
      this.videoStateSubscriptions = new Subscription();
      this.loadIframes();
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.videoStateSubscriptions.unsubscribe();
  }

  subscribeYoutubeIframeAPIReady() {
    this.subscription.add(this.youtubeAPIService.youtubeIframeAPIReady.subscribe((ready: boolean) => {
      this.youtubeIframeAPIReady = ready;
      if (ready) {
        this.loadIframes();
      }
    }))
  }

  loadIframes() {
    for (let video of (this.userVideos || [])) {
      setTimeout(() => {
        video.player = new YT.Player(video._id, {
          height: this.height,
          width: this.width,
          videoId: this.getVideoId(video),
          playerVars: {
            autoplay: 0,
            modestbranding: 1,
            controls: 1,
            disablekb: 1,
            rel: 0,
            showinfo: 0,
            fs: 0,
            playsinline: 1

          },
          events: {
            'onStateChange': (event) => {
              this.onPlayerStateChange(event);
            },
            'onError': (event) => {
              this.onPlayerError(event);
            },
            'onReady': (event) => {
              this.videoStateSubscriptions.add(video.stateObservable.subscribe((state: string) => {
                switch(state) {
                  case 'stop': video && video.player && video.player.stopVideo(); break;
                  case 'pause': video && video.player && video.player.pauseVideo(); break;
                  case 'play': video && video.player && video.player.playVideo(); break;
                }
              }));
              this.onPlayerReady(event);
            },
          }
        });        
      }, 0);
      
    }
  }

  onPlayerError(event) {
  }

  onPlayerReady(event) {
  }

  onPlayerStateChange(event) {
  }

  makeid(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }

  getVideoId(clientVideo) {
    if (!clientVideo) return '';
    let splitted = (clientVideo.url || '').split('?');
    return new URLSearchParams(splitted[splitted.length - 1]).get('v');
  }
}