import { Component, OnInit, OnDestroy } from '@angular/core';
import { YoutubeAPIService } from 'src/app/shared/services/youtube-api.service';

@Component({
  selector: 'app-visitor-component',
  templateUrl: './visitor.component.html',
  styleUrls: ['./visitor.component.css']
})

export class VisitorComponent implements OnInit, OnDestroy {
  constructor(private youtubeAPIService: YoutubeAPIService) {

  }

  ngOnInit() {

  }

  ngOnDestroy() {
    
  }

  signIn() {
    this.youtubeAPIService.handleAuthClick();
  }
}