import { NgModule } from '@angular/core';
import { VisitorComponent } from './visitor.component';
import { VisitorRoutingModule } from './visitor-routing.module';
import { VisitorGuard } from './visitor-guard.service';
@NgModule({
  imports: [
    VisitorRoutingModule,
  ],
  declarations: [
    VisitorComponent
  ],
  providers: [
    VisitorGuard
  ]
})
export class VisitorModule { }
