import { CanActivate, Route, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/shared/services/auth.service';

@Injectable()
export class VisitorGuard implements CanActivate {
  constructor(private authService: AuthService,
    private router: Router) {
  }

  canActivate() {
    if (!this.authService.isSignedUser()) {
      return true;
    }
    this.router.navigate(['/signed']);
    return false;
  }
}