import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VisitorComponent } from './visitor.component';
import { VisitorGuard } from './visitor-guard.service';

const routes: Routes = [
  {
    path: '',
    component: VisitorComponent,
    canActivate: [
      VisitorGuard
    ],
    children: [
      // {
      //   path: 'login',
      //   loadChildren: './routes/login/login.module#LoginModule'
      // },
      // {
      //   path: 'register',
      //   loadChildren: './routes/register/register.module#RegisterModule'
      // },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisitorRoutingModule { }
