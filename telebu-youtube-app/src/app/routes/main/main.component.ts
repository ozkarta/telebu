import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { YoutubeAPIService } from 'src/app/shared/services/youtube-api.service';
import { AuthService } from 'src/app/shared/services/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit, OnDestroy {
  public isSignedInGoogle: boolean = false;
  public currentUser: any = null;

  private subscription: Subscription = new Subscription();
  constructor(
    private router: Router,
    private ref: ChangeDetectorRef,
    private authService: AuthService,
    private youtubeAPIService: YoutubeAPIService
  ) {}

  ngOnInit() {
    this.subscribeUser();
    this.subscribeUserStatus();

    this.youtubeAPIService.load();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.youtubeAPIService.revokeAccess();
  }

  subscribeUserStatus() {
    this.subscription.add(this.authService.isSigned.subscribe((isSignedInGoogle: boolean) => {
      this.isSignedInGoogle = isSignedInGoogle;
      if (this.isSignedInGoogle) {
        this.router.navigate(['/signed']);
      } else {
        this.router.navigate(['/']);
      }
    }));
  }

  subscribeUser() {
    this.subscription.add(this.authService.currentUser.subscribe((currentuser: any) => this.currentUser = currentuser));
  }

  // =======================================

  signIn() {
    this.youtubeAPIService.handleAuthClick();
  }

  logOut() {
    this.youtubeAPIService.handleAuthClick();
  }
}