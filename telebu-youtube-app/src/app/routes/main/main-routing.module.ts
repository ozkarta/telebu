import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    resolve: {
    },
    children: [
      {
        path: '',
        loadChildren: './routes/visitor/visitor.module#VisitorModule'
      },
      {
        path: 'signed',
        loadChildren: './routes/signed/signed.module#SignedModule'
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
