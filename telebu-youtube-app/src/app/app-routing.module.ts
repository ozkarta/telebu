import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

const routes = [
  {
    path: '',
    canActivate: [],
    children: [
      {
        path: '',
        loadChildren: './routes/main/main.module#MainModule'
      },      
    ]
  },
  // {
  //   path: '**',
  //   loadChildren: './routes/page-not-found/page-not-found.module#PageNotFoundModule'
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload', scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled'})],
  declarations: [],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}