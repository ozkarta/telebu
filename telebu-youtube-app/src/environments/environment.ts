// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  serverURL: 'http://localhost:3000',
  google: { 
    web : { 
      "client_id": "747779017929-lq0vteprfakp1eocf7fj9j9vu4ekv536.apps.googleusercontent.com", 
      "project_id": "telebu-app", 
      "auth_uri": "https://accounts.google.com/o/oauth2/auth", 
      "token_uri": "https://oauth2.googleapis.com/token", 
      "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs", 
      "client_secret": "SmGeusR4Gx5U_xEdfJ37YHY6", 
      "redirect_uris": ["http://localhost:4200"], 
      "javascript_origins": ["http://localhost:4200"],
      apiKey: 'AIzaSyDIFc_4rozD6c1WxBNu0l2-W7bCLHf9btg',
    } 
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
